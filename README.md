# Итоговый проект факультета Geekbrains DevOps

## Описание проекта

Высокодоступное приложение Wordpress, реализуемое в облаке VK Cloud, использует кластер Kubernetes и  для хранения контента. Входящий трафик защищается Cloudflare DNS и распределяется по нодам пула stateless приложений. Мониторинг и алертинг осуществляется Prometheus+Grafana. Архитектурная схемач:

### Домен

В проекте для фильтрации трафика применяется сервис Cloudflare, для которого следует выполнить делегирование своего доменного имени. В консоли доменного регистратора необходимо изменить NS сервера на [NS сервера Clouflare](https://developers.cloudflare.com/dns/zone-setups/full-setup/setup/)

#### Пошаговая инструкция для развертывания инфраструктуры

Для данного проекта используется Ansible(для развертывания окружения),Terraform(для создания инфраструктуры на основе облака VK Cloud ) и манифесты для поднятия Pod-ов на кластрере k8s.

1. Клонировать репозиторий

   ```
   git clone https://gitlab.com/Drag2517/gb_teamproject
   ```

2. Установите Ansible :

   sudo apt update

   sudo apt install ansible

3. Скачайте данны репозиторий себе на администраторскую машину на которой должен осуществляться доступ к приложению

4. [Настройте Terraform для работы на VK Cloud](https://mcs.mail.ru/docs/manage/terraform/quick-start#tabpanel-31-2-5)

5. Заходим в папку Terraform, запускаем командами:

   terraform init

   terraform apply

6. [Для подключения к кластеру Kubernetes используйте данную ссылку](https://mcs.mail.ru/docs/base/k8s/connect/kubectl#podklyuchenie-k-klasteru)

   ## Уведомления от GitLab

   Для получения уведомлений о событиях `push` и `pipeline` используется Telegram Bot. Чтобы воспользоваться уведомлениями в группе необходимо выполнить следующие шаги:

   1. Добавить Telegram Bot [@git_events_bot](https://t.me/git_events_bot) в групповой чат или написать сообщение `/start` в личном сообщении
   2. Бот отправит ссылку на Webhook
   3. Скопировать полученную ссылку на Telegram Webhook
   4. Перейти в репозиторий проекта Gitlab, открыть раздел `Settings -> Webhooks`
   5. Вставить скопированный Webhook URL и выбрать нужные триггеры
   6. Сохранить изменения, нажав кнопку `Add webho
