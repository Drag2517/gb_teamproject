data "vkcs_kubernetes_clustertemplate" "ct" {
  version = "1.23"
}

resource "vkcs_kubernetes_cluster" "k8s-cluster" {
  depends_on = [
    vkcs_networking_router_interface.k8s,
  ]

  name                = "k8s-cluster"
  cluster_template_id = data.vkcs_kubernetes_clustertemplate.ct.id
  master_flavor       = data.vkcs_compute_flavor.k8s.id
  master_count        = 1

  network_id          = vkcs_networking_network.k8s.id
  subnet_id           = vkcs_networking_subnet.k8s-subnetwork.id
  floating_ip_enabled = true
  availability_zone   = "GZ1"
  insecure_registries = ["1.2.3.4"]
 
  labels = {
    # Нужные аддоны
    docker_registry_enabled = true
    ingress_controller="nginx"
  }
}

resource "vkcs_kubernetes_node_group" "k8s-node-group" {
  name = "k8s-node-group"
  node_count = 2
  cluster_id = vkcs_kubernetes_cluster.k8s-cluster.id
  flavor_id = data.vkcs_compute_flavor.k8s.id
  autoscaling_enabled = true
  min_nodes = 2
  max_nodes = 4
}
